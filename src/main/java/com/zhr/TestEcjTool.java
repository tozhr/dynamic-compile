package com.zhr;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Test
 *
 * @author zhr
 */
public class TestEcjTool {
    public static void main(String[] args) throws IOException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        String javaCode = "public class    HelloWorld   {\n" +
                "\tprivate String name;\n" +
                "\tpublic String getName(){\n" +
                "\t\treturn this.name;\n" +
                "\t}\n" +
                "\tpublic void setName(String name){\n" +
                "\t\tthis.name=name;\n" +
                "\t}\n" +
                "}";
        Class<?> clazz = EcjTool.compileJavaCode(javaCode, null);
        Object o = clazz.newInstance();
        Method getName = clazz.getDeclaredMethod("getName");
        Method setName = clazz.getDeclaredMethod("setName", String.class);
        setName.invoke(o, "小明");
        Object invoke = getName.invoke(o);
        System.out.println(invoke);
    }
}
